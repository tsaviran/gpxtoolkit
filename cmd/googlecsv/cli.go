package main

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"time"

	tk "gitlab.com/tsaviran/gpxtoolkit"
	"gitlab.com/tsaviran/gpxtoolkit/csv"
)

type location struct {
	TSS  string `json:"timestampMs"`
	Lat7 int64  `json:"latitudeE7"`
	Lon7 int64  `json:"longitudeE7"`
}

func (l *location) time() time.Time {
	tms, err := strconv.ParseInt(l.TSS, 10, 64)
	if err != nil {
		panic(err)
	}
	return time.UnixMilli(tms)
}

func (l *location) lat() float64 {
	return float64(l.Lat7) / 10_000_000.0
}

func (l *location) lon() float64 {
	return float64(l.Lon7) / 10_000_000.0
}

func (l *location) Point() tk.Point {
	return tk.Point{
		Lat:     l.lat(),
		Lon:     l.lon(),
		Time:    l.time(),
		HasLoc:  true,
		HasTime: true,
	}
}

type history struct {
	Locations []location `json:"locations"`
}

func main() {
	d, err := os.ReadFile(os.Args[1])
	if err != nil {
		panic(err)
	}
	var hist history
	if err := json.Unmarshal(d, &hist); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	for _, l := range hist.Locations {
		csv.FprintPoint(os.Stdout, l.Point())
		fmt.Println() // finish csv print
	}
}
