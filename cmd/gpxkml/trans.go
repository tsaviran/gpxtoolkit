package main

import (
	"fmt"
	"io"
	"os"
	"time"

	tk "gitlab.com/tsaviran/gpxtoolkit"
	"gitlab.com/tsaviran/gpxtoolkit/gpx"
	"gitlab.com/tsaviran/gpxtoolkit/kml"

	"github.com/StefanSchroeder/Golang-Ellipsoid/ellipsoid"
)

type pathType int

const (
	linePath pathType = iota
	trackPath
)

type trans struct {
	multi bool

	name string // input name for kml folder
	nGPX int    // number of GPX entities per transform call

	gpxDist float64 // distance for current GPX
	totDist float64 // total distance for this transform call
	totTime float64

	kml      *kml.Writer
	gosax    gpx.GoSAXReader
	stdlib   gpx.StdlibReader
	pathType pathType

	geo       ellipsoid.Ellipsoid
	prevTime  time.Time
	firstTime time.Time
	prevPt    tk.Point

	startGPXCB func(i int)              // callback for GPX start
	endGPXCB   func(dist, time float64) // callback for GPX end
}

// newTrans creates new GPX-to-KML transformer.
func newTrans(w io.Writer) *trans {
	kml := kml.NewWriter(w)

	t := trans{
		kml:      &kml,
		pathType: linePath,
		multi:    true,
	}

	t.gosax.DocCB = t.newGPXDoc
	t.gosax.TrackCB = t.newTrack
	t.gosax.PointCB = t.newPoint
	t.stdlib.DocCB = t.newGPXDoc
	t.stdlib.TrackCB = t.newTrack
	t.stdlib.PointCB = t.newPoint

	t.geo = ellipsoid.Init("WGS84", ellipsoid.Degrees, ellipsoid.Meter,
		ellipsoid.LongitudeIsSymmetric, ellipsoid.BearingIsSymmetric)

	return &t
}

// resetState prepares transformer for a new GPX data source.
func (t *trans) resetState() {
	t.name = ""
	t.totDist = 0.0
	t.gpxDist = 0.0
	t.nGPX = 0
	t.prevTime = time.Time{}
	t.firstTime = time.Time{}
	t.prevPt.Invalidate()
}

// useLines sets output mode to KML lines.
func (t *trans) useLines() {
	t.pathType = linePath
}

// useTracks sets output mode to KML tracks.
func (t *trans) useTracks() {
	t.pathType = trackPath
}

// finalise finalises output KML. Finalise must be called exactly once during
// the lifetime of trans or behaviour is undefined.
func (t *trans) finalise() {
	t.kml.Finalise()
	t.kml = nil
}

// transformStream translates GPX data from io to KML. It returns total
// distance in metres and time in seconds.
func (t *trans) transformStream(r io.Reader, name string) (float64, float64) {
	t.resetState()
	t.name = name
	// Parse may trigger several callbacks. While GPX reader has a callback
	// for new GPX document, the end is implicit. The start of the second
	// document implies the first one has ended, and returning from Parse
	// implies the last one has ended.
	err := t.stdlib.Parse(r)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
	t.endGPX()
	return t.totDist, t.totTime
}

// transformFile translates GPX data in input filename to KML. It returns total
// distance in metres and time in seconds.
func (t *trans) transformFile(filename string) (float64, float64) {
	t.resetState()
	t.name = filename
	err := t.gosax.ParseFile(filename)
	// ParseFile may trigger several callbacks. See transformStream.
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
	t.endGPX()
	return t.totDist, t.totTime
}

func (t *trans) endGPX() {
	t.totDist += t.gpxDist
	var el float64
	if !t.firstTime.IsZero() && !t.prevTime.IsZero() {
		el = t.prevTime.Sub(t.firstTime).Seconds()
		t.totTime += el
	}
	if t.endGPXCB != nil {
		t.endGPXCB(t.gpxDist, el)
	}
}

// newGPXDoc is called when GPX reader encounters a new GPX entity
func (t *trans) newGPXDoc() {
	if t.nGPX > 0 {
		t.endGPX()
	}

	t.nGPX++
	t.gpxDist = 0.0
	t.prevTime = time.Time{}
	t.firstTime = time.Time{}

	if t.multi && t.nGPX >= 2 {
		t.kml.NewFolder(fmt.Sprintf("%s (%d)", t.name, t.nGPX))
	} else if t.nGPX == 1 {
		t.kml.NewFolder(t.name)
	}
	if t.startGPXCB != nil {
		t.startGPXCB(t.nGPX - 1)
	}
}

func (t *trans) newTrack(name string) {
	switch t.pathType {
	case trackPath:
		t.kml.NewTrack(name)
	case linePath:
		t.kml.NewLine(name)
	}
}

func (t *trans) newPoint(pt *tk.Point) {
	if t.prevPt.HasLoc {
		d, _ := t.geo.To(t.prevPt.Lat, t.prevPt.Lon, pt.Lat, pt.Lon)
		t.gpxDist += d
	}
	if t.firstTime.IsZero() && !pt.Time.IsZero() {
		t.firstTime = pt.Time
	}
	if !pt.Time.IsZero() {
		t.prevTime = pt.Time
	}
	t.prevPt = *pt
	t.kml.NewPoint(pt)
}
