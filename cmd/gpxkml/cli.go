package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

type cli struct {
	tr *trans

	timeS float64
	distM float64

	fn     string
	iFile  int
	iGPX   int
	nFiles int
}

var outputMode = "auto"
var emptyTrack = false
var emptySegment = false
var listFiles = false
var printStats = false

func init() {
	flag.StringVar(&outputMode, "mode", "auto",
		"output mode, track or line")
	flag.BoolVar(&emptyTrack, "empty-track", false,
		"produce empty tracks")
	flag.BoolVar(&emptyTrack, "empty-segment", false,
		"produce empty segments")
	flag.BoolVar(&listFiles, "list-files", false,
		"list files as they are processed")
	flag.BoolVar(&printStats, "print-stats", false,
		"print statistics")
}

func main() {
	flag.Parse()

	w := bufio.NewWriter(os.Stdout)
	cli := cli{tr: newTrans(w)}

	if outputMode == "track" || (outputMode == "auto" && flag.NArg() < 2) {
		cli.tr.useTracks()
	} else if outputMode == "line" {
		cli.tr.useLines()
	}

	var distM float64

	if listFiles {
		cli.tr.startGPXCB = cli.startGPX
		cli.tr.endGPXCB = cli.endGPX
	}

	if flag.NArg() == 0 {
		cli.nFiles = 1
		cli.fn = "stdin"
		d, _ := cli.tr.transformStream(os.Stdin, "stdin")
		distM += d
	} else {
		cli.nFiles = len(flag.Args())
		for i, fn := range flag.Args() {
			cli.fn = fn
			cli.iFile = i
			d := cli.transformFile(fn, i, flag.NArg())
			distM += d
		}
	}
	cli.tr.finalise()
	w.Flush()

	if printStats {
		fmt.Fprintf(os.Stderr, "total distance: %.1f km\n",
			distM/1000.0)
	}
}

func (cli *cli) startGPX(i int) {
	cli.iGPX++
	if !listFiles {
		return
	}
	fmt.Fprintf(os.Stderr, "%d", cli.iFile+1)
	if cli.iGPX-1 != cli.iFile {
		fmt.Fprintf(os.Stderr, "(%d)", cli.iGPX)
	}
	if cli.nFiles >= 1 {
		fmt.Fprintf(os.Stderr, "/%d", cli.nFiles)
	}
	fmt.Fprintf(os.Stderr, " %s", cli.fn)
}

func (cli *cli) endGPX(dist, time float64) {
	cli.distM += dist
	cli.timeS += time

	if listFiles && printStats {
		distKM := dist / 1000.0
		fmt.Fprintf(os.Stderr, "  %.1f km", distKM)
		timeHr := time / 3600.0
		if time > 0.0 {
			fmt.Fprintf(os.Stderr, " / %.2f h", timeHr)
			fmt.Fprintf(os.Stderr, " / %.2f km/h", distKM/timeHr)
		}
	}
	if listFiles {
		fmt.Fprintf(os.Stderr, "\n")
	}
}

func (cli *cli) transformFile(fn string, i, n int) float64 {
	cli.iFile = i
	d, _ := cli.tr.transformFile(fn)
	return d
}
