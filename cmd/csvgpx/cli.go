package main

import (
	"bufio"
	"os"

	tk "gitlab.com/tsaviran/gpxtoolkit"
	"gitlab.com/tsaviran/gpxtoolkit/csv"
	"gitlab.com/tsaviran/gpxtoolkit/gpx"
)

func main() {
	bio := bufio.NewWriter(os.Stdout)
	defer bio.Flush()
	w := gpx.NewWriter(bio)
	defer w.Finalise()

	csv.EachPoint(os.Stdin, func(pt tk.Point) {
		w.WritePoint(pt)
	})
}
