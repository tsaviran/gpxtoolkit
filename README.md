# gpxtoolkit

Collection of tools to deal with GPX files.


## Tools


### gpxkml

Translate GPX files to KML files suitable for viewing in Google Earth.


### googlecsv

Translate Google location history XML into gpxtoolkit CSV.


## csvgpx

Translate gpxtoolkit CSV into GPX.


## Compiling

`go build` in tool directory.
