package kml

// Writer creates (mostly) flat KML files with no nested folders etc.
//
// May write "default", "d-normal", and "d-highlight" styles.

import (
	"fmt"
	"io"
	"time"

	tk "gitlab.com/tsaviran/gpxtoolkit"

	"gitlab.com/tsaviran/xmlstream"
)

// Writer is a streaming KML document writer.
type Writer struct {
	xml       *xmlstream.Writer
	doc       bool
	folder    bool
	track     bool
	line      bool
	placemark bool
	Style     string
	oldTS     time.Time
}

// XML returns used xmlstream.Writer.
func (k *Writer) XML() *xmlstream.Writer {
	return k.xml
}

// NewWriter creates nek KML writer.
func NewWriter(io io.Writer) Writer {
	return Writer{
		xml:       xmlstream.NewWriter(io),
		doc:       false,
		folder:    false,
		track:     false,
		line:      false,
		placemark: false,
		Style:     "default",
	}
}

// Finalise closes an open KML document.
func (k *Writer) Finalise() {
	k.maybeEndDocument()
	k.xml.Finalise()
}

// document

func (k *Writer) newDocument() {
	k.xml.StartElement("kml")
	k.xml.AddAttribute("xmlns", "http://www.opengis.net/kml/2.2")
	k.xml.AddAttribute("xmlns:gx", "http://www.google.com/kml/ext/2.2")
	k.xml.StartElement("Document")
	k.xml.OpenElement()
	k.doc = true
}

func (k *Writer) maybeNewDocument() bool {
	if k.doc {
		return true
	}
	k.newDocument()
	return false
}

func (k *Writer) maybeNewStdDocument() {
	if k.maybeNewDocument() {
		return
	}

	k.xml.WriteRaw(`
	<Style id="d-normal">
		<IconStyle>
			<scale>1</scale>
			<hotSpot x="20" y="2" xunits="pixels" yunits="pixels"/>
		</IconStyle>
		<LineStyle>
			<color>ff0000ff</color>
			<width>3</width>
		</LineStyle>
	</Style>
	<Style id="d-highlight">
		<IconStyle>
			<scale>1.3</scale>
			<Icon>
				<href>http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png</href>
			</Icon>
			<hotSpot x="20" y="2" xunits="pixels" yunits="pixels"/>
		</IconStyle>
		<LineStyle>
			<color>ff0000ff</color>
			<width>3</width>
		</LineStyle>
	</Style>
	<StyleMap id="default">
		<Pair>
			<key>normal</key>
			<styleUrl>#d-normal</styleUrl>
		</Pair>
		<Pair>
			<key>highlight</key>
			<styleUrl>#d-highlight</styleUrl>
		</Pair>
	</StyleMap>
`)
}

func (k *Writer) endDocument() {
	k.xml.EndElement() // Document
	k.doc = false
	k.xml.EndElement() // kml
}

func (k *Writer) maybeEndDocument() {
	k.maybeEndFolder()
	if !k.doc {
		return
	}
	k.endDocument()
}

// style

// WriteStyle writes raw string as kml Style.
func (k *Writer) WriteStyle(s string) {
	k.maybeNewDocument()
	k.xml.WriteRaw(s)
}

// folder

// NewFolder creates a nek folder
func (k *Writer) NewFolder(name string) {
	k.maybeNewStdDocument()
	k.maybeEndFolder()

	k.xml.StartElement("Folder")
	if name != "" {
		k.xml.ElementCData("name", name)
	}
	k.folder = true
	k.resetTS()
}

func (k *Writer) maybeNewFolder(name string) {
	if k.folder {
		return
	}
	k.NewFolder(name)
}

func (k *Writer) endFolder() {
	k.xml.EndElement() // Folder
	k.folder = false
}

func (k *Writer) maybeEndFolder() {
	k.maybeEndPath()
	k.maybeEndLine()
	if !k.folder {
		return
	}
	k.endFolder()
}

// placemark

// NewPlacemarkWithStyle creates a new placemark with given style.
func (k *Writer) NewPlacemarkWithStyle(name, style string) {
	k.xml.StartElement("Placemark")
	k.xml.ElementCData("styleUrl", "#"+style)
	if name != "" {
		k.xml.ElementCData("name", name)
	}
	k.placemark = true
}

// NewPlacemark creates a new placemark.
func (k *Writer) NewPlacemark(name string) {
	k.NewPlacemarkWithStyle(name, k.Style)
}

func (k *Writer) maybeNewPlacemark(name string) {
	if k.placemark {
		return
	}
	k.NewPlacemark(name)
}

// EndPlacemark closes off a placemark.
func (k *Writer) EndPlacemark() {
	k.xml.EndElement() // Placemark
	k.placemark = false
}

func (k *Writer) maybeEndPlacemark() {
	if !k.placemark {
		return
	}
	k.EndPlacemark()
}

// track

// NewTrack creates a nek track path.
func (k *Writer) NewTrack(name string) {
	k.maybeEndPath()
	k.maybeNewFolder(name)
	k.NewPlacemark(name)

	k.xml.StartElement("gx:Track")
	k.track = true
}

func (k *Writer) endTrack() {
	k.xml.EndElement() // gx:Track
	k.EndPlacemark()
	k.track = false
}

func (k *Writer) maybeEndTrack() {
	if !k.track {
		return
	}
	k.endTrack()
}

func (k *Writer) trackPoint(pt *tk.Point) {
	k.maybeNewFolder("")
	k.maybeNewPlacemark("")

	if ts, has := k.getTS(pt); has {
		k.xml.ElementCData("when", ts.Format(time.RFC3339))
	}

	var coord string
	if pt.HasElev {
		coord = fmt.Sprintf("%f %f %f", pt.Lon, pt.Lat, pt.Elev)
	} else {
		coord = fmt.Sprintf("%f %f", pt.Lon, pt.Lat)
	}
	k.xml.ElementCData("gx:coord", coord)
}

// line

// NewLine creates a nek line path.
func (k *Writer) NewLine(name string) {
	k.maybeEndPath()
	k.maybeNewFolder(name)
	k.NewPlacemark(name)

	k.xml.StartElement("LineString")
	k.xml.ElementCData("extrude", "0")
	k.xml.ElementCData("tessellate", "0")
	k.xml.ElementCData("altitudeMode", "clampToGroup")
	k.xml.StartElement("coordinates")
	k.line = true
}

func (k *Writer) endLine() {
	k.xml.EndElement() // coordinates
	k.xml.EndElement() // LineString
	k.EndPlacemark()
	k.line = false
}

func (k *Writer) maybeEndLine() {
	if !k.line {
		return
	}
	k.endLine()
}

func (k *Writer) linePoint(pt *tk.Point) {
	if pt.HasElev {
		k.xml.WriteCData(fmt.Sprintf("%.6f,%.6f,%.3f\n",
			pt.Lon, pt.Lat, pt.Elev))
	} else {
		k.xml.WriteCData(fmt.Sprintf("%.6f,%.6f\n",
			pt.Lon, pt.Lat))
	}
}

// path

func (k *Writer) inPath() bool {
	return k.track || k.line
}

func (k *Writer) maybeEndPath() {
	if k.line {
		k.endLine()
		return
	}
	if k.track {
		k.endTrack()
	}
}

func (k *Writer) resetTS() {
	k.oldTS = time.Time{}
}

// point

// NewPoint adds a nek point to a path.
func (k *Writer) NewPoint(pt *tk.Point) {
	if !k.inPath() {
		panic("not in path")
	}

	if k.line {
		k.linePoint(pt)
	} else {
		k.trackPoint(pt)
	}
}

func (k *Writer) getTS(pt *tk.Point) (time.Time, bool) {
	if pt.HasTime {
		k.oldTS = pt.Time
		return pt.Time, true
	} else if !k.oldTS.IsZero() {
		return k.oldTS, true
	}
	return time.Time{}, false
}
