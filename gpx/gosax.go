package gpx

import (
	"io"
	"io/ioutil"
	"os"

	"github.com/eliben/gosax"
)

// GoSAXReader is a high performace GPX track reader using GoSAX (libxml2) that
// streams data, but can only work on existing files.
type GoSAXReader struct {
	ReaderCallbacks
}

// Parse parses GPX data. Data is first read and written to a temporary file.
func (g GoSAXReader) Parse(r io.Reader) error {
	t, err := ioutil.TempFile(os.TempDir(), "gosax")
	if err != nil {
		return err
	}
	defer os.Remove(t.Name())
	_, err = io.Copy(t, r)
	if err != nil {
		return err
	}
	return g.ParseFile(t.Name())
}

// ParseFile parses a GPX file.
func (g GoSAXReader) ParseFile(filename string) error {
	p := newParser()
	cb := gosax.SaxCallbacks{
		StartElement: p.start,
		EndElement:   p.end,
		Characters:   p.chars,
	}
	p.DocCB = g.DocCB
	p.TrackCB = g.TrackCB
	p.SegmentCB = g.SegmentCB
	p.PointCB = g.PointCB
	return gosax.ParseFile(filename, cb)
}
