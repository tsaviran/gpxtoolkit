package gpx

import (
	"encoding/xml"
	"io"

	tk "gitlab.com/tsaviran/gpxtoolkit"
)

// StdlibReader is a streaming GPX track reader using Go stdlib.
type StdlibReader struct {
	parser parser
	dec    *xml.Decoder

	ReaderCallbacks
}

func (r *StdlibReader) docCB() {
	if r.ReaderCallbacks.DocCB != nil {
		r.ReaderCallbacks.DocCB()
	}
}

func (r *StdlibReader) trackCB(name string) {
	if r.ReaderCallbacks.TrackCB != nil {
		r.ReaderCallbacks.TrackCB(name)
	}
}

func (r *StdlibReader) segmentCB() {
	if r.ReaderCallbacks.SegmentCB != nil {
		r.ReaderCallbacks.SegmentCB()
	}
}

func (r *StdlibReader) pointCB(pt *tk.Point) {
	if r.ReaderCallbacks.PointCB != nil {
		r.ReaderCallbacks.PointCB(pt)
	}
}

func (r *StdlibReader) startEl(el xml.StartElement) error {
	switch el.Name.Local {
	case "trkpt":
		attrs := []string{}
		for _, a := range el.Attr {
			attrs = append(attrs, a.Name.Local, a.Value)
		}
		r.parser.start("trkpt", attrs)
	default:
		r.parser.start(el.Name.Local, []string{}) // TODO
	}
	return nil
}

func (r *StdlibReader) endEl(el xml.EndElement) error {
	r.parser.end(el.Name.Local)
	return nil
}

func (r *StdlibReader) charData(el xml.CharData) error {
	r.parser.chars(string(el))
	return nil
}

// Parse parses GPX data.
func (r StdlibReader) Parse(rd io.Reader) error {
	// all assignments are local
	r.parser = newParser()
	r.parser.DocCB = r.docCB
	r.parser.TrackCB = r.trackCB
	r.parser.SegmentCB = r.segmentCB
	r.parser.PointCB = r.pointCB
	r.dec = xml.NewDecoder(rd)
	for {
		tok, err := r.dec.Token()
		if tok == nil || err == io.EOF {
			return nil
		} else if err != nil {
			return err
		}

		switch ty := tok.(type) {
		case xml.StartElement:
			err = r.startEl(ty)
		case xml.EndElement:
			err = r.endEl(ty)
		case xml.CharData:
			err = r.charData(ty)
		default:
		}
		if err != nil {
			return err
		}
	}
}
