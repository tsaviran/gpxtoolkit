package gpx

import (
	"io"
	"strconv"
	"time"

	tk "gitlab.com/tsaviran/gpxtoolkit"
)

type elementType int

const (
	metaNameEl elementType = iota
	metaTimeEl
	trkNameEl
	ptEleEl
	ptTimeEl
	noneEl
)

type readerCBs struct {
	Track   func(name string)
	Segment func()
	Point   func(pt *tk.Point)
}

type reader interface {
	Parse(r io.Reader) error
}

// DocCB is a callback function for new GPX document.
type DocCB func()

// TrackCB is a callback function for new tracks.
type TrackCB func(name string)

// SegmentCB is a callback function for new track segments.
type SegmentCB func()

// PointCB is a callback function for new track points.
type PointCB func(pt *tk.Point)

// ReaderCallbacks is a set of standard reader callbacks.
type ReaderCallbacks struct {
	DocCB     DocCB
	TrackCB   TrackCB
	SegmentCB SegmentCB
	PointCB   PointCB
}

type parser struct {
	ctxMeta bool // name, title
	ctxTrk  bool // name
	charCtx elementType

	wantEle         bool
	wantTime        bool
	wantEmptyTrk    bool
	wantEmptyTrkSeg bool

	trkSent bool
	trkName string

	trkSegSent bool

	pt tk.Point

	ReaderCallbacks
}

func newParser() parser {
	return parser{
		ctxMeta: false,
		ctxTrk:  false,
		charCtx: noneEl,

		wantEle:  true,
		wantTime: true,

		wantEmptyTrk:    true,
		wantEmptyTrkSeg: true,
	}
}

func (p *parser) start(name string, attrs []string) {
	if name == "gpx" {
		p.startGPX()
	} else if name == "metadata" {
		p.ctxMeta = true
	} else if name == "name" {
		if p.ctxMeta {
			p.charCtx = metaNameEl
		} else if p.ctxTrk {
			p.charCtx = trkNameEl
		}
	} else if name == "trk" {
		p.startTrk()
	} else if name == "trkseg" {
		p.startTrkSeg()
	} else if name == "trkpt" {
		p.startTrkPt(attrs)
	} else if name == "ele" {
		p.charCtx = ptEleEl
	} else if name == "time" {
		p.charCtx = ptTimeEl
	} else {
		p.charCtx = noneEl
	}
}

func (p *parser) startGPX() {
	if p.DocCB != nil {
		p.DocCB()
	}
}

func (p *parser) startTrk() {
	p.ctxMeta = false
	p.ctxTrk = true
	p.trkSent = false
	p.trkName = ""
}

func (p *parser) startTrkSeg() {
	p.trkSegSent = false
	if p.wantEmptyTrkSeg {
		p.ensureTrkSeg() // implies ensureTrk
	} else if p.wantEmptyTrk {
		p.ensureTrk()
	}
}

func (p *parser) startTrkPt(attrs []string) {
	if p.pt.HasLoc {
		p.endTrkPt()
	}

	var err error
	p.pt.Invalidate()
	hasLat := false
	hasLon := false
	for i := 0; i < len(attrs); i += 2 {
		if attrs[i] == "lat" {
			p.pt.Lat, err = strconv.ParseFloat(attrs[i+1], 64)
			if err != nil {
				return
			}
			hasLat = true
		} else if attrs[i] == "lon" {
			p.pt.Lon, err = strconv.ParseFloat(attrs[i+1], 64)
			if err != nil {
				return
			}
			hasLon = true
		}
		if hasLat && hasLon {
			p.pt.HasLoc = true
			break
		}
	}
}

func (p *parser) ensureTrk() {
	if p.trkSent {
		return
	}
	if p.TrackCB != nil {
		p.TrackCB(p.trkName)
	}
	p.trkSent = true
}

func (p *parser) ensureTrkSeg() {
	p.ensureTrk()
	if p.trkSegSent {
		return
	}
	if p.SegmentCB != nil {
		p.SegmentCB()
	}
	p.trkSegSent = true
}

func (p *parser) endTrkPt() {
	if !p.pt.HasLoc {
		return
	}
	p.ensureTrkSeg() // implies ensureTrk

	if p.PointCB != nil {
		p.PointCB(&p.pt)
	}
	p.pt.Invalidate()
}

func (p *parser) end(name string) {
	p.charCtx = noneEl
	if name == "metadata" {
		p.ctxMeta = false
	} else if name == "trkpt" {
		p.endTrkPt()
	} else if name == "trkseg" {
		if p.pt.HasLoc {
			p.endTrkPt()
		}
	}
}

func (p *parser) chars(contents string) {
	switch p.charCtx {
	case trkNameEl:
		p.trkName = contents
		if p.wantEmptyTrk {
			p.ensureTrk()
		}
	case ptEleEl:
		p.ptEle(&contents)
	case ptTimeEl:
		p.ptTime(&contents)
	// case metaNameEl:
	// case metaTimeEl:
	// case noneEl:
	default:
		// nop
	}
}

func (p *parser) ptEle(ele *string) {
	if !p.wantEle {
		return
	}
	t, err := strconv.ParseFloat(*ele, 64)
	if err == nil {
		p.pt.Elev = t
		p.pt.HasElev = true
	}
}

func (p *parser) ptTime(ele *string) {
	if !p.wantTime {
		return
	}
	t, err := time.Parse(time.RFC3339, *ele)
	if err == nil {
		p.pt.Time = t
		p.pt.HasTime = true
		return
	}
	// Z-less timestamp
	special := time.RFC3339[0 : len(time.RFC3339)-6]
	t, err = time.Parse(special, *ele)
	if err == nil {
		p.pt.Time = t
		p.pt.HasTime = true
		return
	}
}
