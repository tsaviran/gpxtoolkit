package gpx

import (
	"bufio"
	"fmt"
	"io"
	"time"

	tk "gitlab.com/tsaviran/gpxtoolkit"
	"gitlab.com/tsaviran/xmlstream"
)

// Writer is a streaming GPX document writer.
type Writer struct {
	xml    *xmlstream.Writer
	g      bool
	trk    bool
	trkseg bool
}

// NewWriter creates a new GPX writer.
func NewWriter(io io.Writer) *Writer {
	bio := bufio.NewWriter(io)
	return &Writer{xml: xmlstream.NewWriter(bio)}
}

// Finalise finalises writing a GPX stream.
func (g *Writer) Finalise() {
	g.maybeEndGPX()
}

// gpx

func (g *Writer) newGPX() {
	g.maybeEndTrk()
	g.xml.StartElement("gpx")
	g.xml.AddAttribute("version", "1.1")
	g.xml.AddAttribute("creator", "gpxtoolkit")
	g.xml.AddAttribute("xmlns", "http://www.topografix.com/GPX/1/1")
	g.xml.AddAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
	g.xml.AddAttribute("xsi:schemaLocation", "http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/g.xsd")
	g.xml.OpenElement()
	g.g = true
}

func (g *Writer) maybeNewGPX() {
	if g.g {
		return
	}
	g.newGPX()
}

func (g *Writer) endGPX() {
	g.maybeEndTrk()
	g.xml.EndElement() // gpx
	g.g = false
}

func (g *Writer) maybeEndGPX() {
	if !g.g {
		return
	}
	g.endGPX()
}

// trk

// NewTrk creates new track optionally with name
func (g *Writer) NewTrk(name *string) {
	g.maybeEndTrk()
	g.maybeNewGPX()

	g.xml.StartElement("trk")
	if name != nil {
		g.xml.ElementCData("name", *name)
	}
	g.trk = true
}

func (g *Writer) maybeNewTrk(name *string) {
	if g.trk {
		return
	}
	g.NewTrk(name)
}

func (g *Writer) endTrk() {
	g.maybeEndTrkseg()
	g.xml.EndElement() // trk
	g.trk = false
}

func (g *Writer) maybeEndTrk() {
	g.maybeEndTrkseg()
	if !g.trk {
		return
	}
	g.endTrk()
}

// trkseg

// NewTrkseg creates a new track segment.
func (g *Writer) NewTrkseg() {
	g.maybeNewTrk(nil)
	g.xml.StartElement("trkseg")
	g.trkseg = true
}

func (g *Writer) maybeNewTrkseg() {
	if g.trkseg {
		return
	}
	g.NewTrkseg()
}

func (g *Writer) endTrkseg() {
	// no inner elements to end
	g.xml.EndElement() // trkseg
	g.trkseg = false
}

func (g *Writer) maybeEndTrkseg() {
	if !g.trkseg {
		return
	}
	g.endTrkseg()
}

// trkpt

// WritePoint writes a new track point.
func (g *Writer) WritePoint(pt tk.Point) {
	g.maybeNewTrkseg()
	g.xml.StartElement("trkpt")
	g.xml.AddAttribute("lat", fmt.Sprintf("%f", pt.Lat))
	g.xml.AddAttribute("lon", fmt.Sprintf("%f", pt.Lon))
	if pt.HasElev {
		g.xml.ElementCData("ele", fmt.Sprintf("%f", pt.Elev))
	}
	if pt.HasTime {
		g.xml.ElementCData("time", pt.Time.Format(time.RFC3339))
	}
	g.xml.EndElement() // trkpt
}
