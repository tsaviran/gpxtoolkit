package gpx

import (
	"fmt"
	"io/ioutil"
	"math"
	"os"
	"strings"
	"testing"
	"time"

	tk "gitlab.com/tsaviran/gpxtoolkit"
)

type track struct {
	name string
}

type segment struct{}

type gpxExpectation struct {
	kind string
	trk  track
	seg  segment
	pt   tk.Point
}

type sampleExpectation struct {
	t   *testing.T
	i   int
	exp []gpxExpectation

	llPrec  float64
	ePrec   float64
	hasName bool
}

func (se *sampleExpectation) track(name string) {
	se.t.Helper()
	defer func() { se.i++ }()

	if se.i >= len(se.exp) {
		se.t.Errorf("too many events")
		return
	}
	if !se.hasName {
		return
	}

	ev := se.exp[se.i]
	if ev.kind != "track" {
		se.t.Errorf("expected %s, got track", ev.kind)
		return
	}

	if name != ev.trk.name {
		se.t.Errorf("expected track %q, got %q", ev.trk.name, name)
	}
}

func (se *sampleExpectation) segment() {
	se.t.Helper()
	defer func() { se.i++ }()

	if se.i >= len(se.exp) {
		se.t.Errorf("too many events")
		return
	}
	ev := se.exp[se.i]
	if ev.kind != "segment" {
		se.t.Errorf("expected %s, got segment", ev.kind)
		return
	}
}

func flEq(a, b, prec float64) bool {
	return math.Abs(a-b) < prec
}

func teq(a, b time.Time) bool {
	d := math.Abs(a.Sub(b).Seconds())
	return d < 0.000000001
}

func (se *sampleExpectation) latLonEq(a, b float64) bool {
	return flEq(a, b, se.llPrec)
}

func (se *sampleExpectation) elevEq(a, b float64) bool {
	return flEq(a, b, se.ePrec)
}

func (se *sampleExpectation) point(pt *tk.Point) {
	se.t.Helper()
	defer func() { se.i++ }()

	if se.i >= len(se.exp) {
		se.t.Errorf("too many events")
		return
	}
	ev := se.exp[se.i]
	if ev.kind != "point" {
		se.t.Errorf("expected %s, got point", ev.kind)
		return
	}

	if ev.pt.HasLoc != pt.HasLoc {
		se.t.Errorf("expected Loc %v, got %v", ev.pt.HasLoc, pt.HasLoc)
	} else if ev.pt.HasLoc && pt.HasLoc {
		if !se.latLonEq(ev.pt.Lat, pt.Lat) {
			se.t.Errorf("expected Lat %v, got %v",
				ev.pt.Lat, pt.Lat)
		}
		if !se.latLonEq(ev.pt.Lon, pt.Lon) {
			se.t.Errorf("expected Lon %v, got %v",
				ev.pt.Lat, pt.Lat)
		}
	}
	if ev.pt.HasElev != pt.HasElev {
		se.t.Errorf("expected Elev %v, got %v",
			ev.pt.HasElev, pt.HasElev)
	} else if ev.pt.HasElev && pt.HasElev {
		if !se.elevEq(ev.pt.Elev, pt.Elev) {
			se.t.Errorf("expected Elev %v, got %v",
				ev.pt.Elev, pt.Elev)
		}
	}
	if ev.pt.HasTime != pt.HasTime {
		se.t.Errorf("expected Time %v, got %v",
			ev.pt.HasTime, pt.HasTime)
	} else if ev.pt.HasTime && pt.HasTime {
		if !teq(ev.pt.Time, pt.Time) {
			se.t.Errorf("expected Time %v, got %v",
				ev.pt.Time, pt.Time)
		}
	}
}

func testSampleReally(t *testing.T, rc func(cbs readerCBs) reader, fn string, llPrec, ePrec float64, hasName bool) {
	t.Helper()

	loc := time.FixedZone("Z", 0)
	exp := []gpxExpectation{
		{
			kind: "track",
			trk:  track{name: "2014-07-22T19:01:04Z"},
		},
		{
			kind: "segment",
		},
		{
			kind: "point",
			pt: tk.Point{
				HasLoc:  true,
				Lat:     60.19359201,
				Lon:     24.93093601,
				HasElev: true,
				Elev:    4.001,
				HasTime: true,
				Time: time.Date(2009, 2, 14, 13, 57, 35,
					0, loc),
			},
		},
		{
			kind: "point",
			pt: tk.Point{
				HasLoc:  true,
				Lat:     60.19357301,
				Lon:     24.93098101,
				HasElev: false,
				HasTime: true,
				Time: time.Date(2009, 2, 14, 13, 57, 39,
					0, loc),
			},
		},
		{
			kind: "point",
			pt: tk.Point{
				HasLoc:  true,
				Lat:     60.19354101,
				Lon:     24.93100701,
				HasElev: true,
				Elev:    -1.00,
				HasTime: true,
				Time: time.Date(2009, 2, 14, 13, 57, 42,
					0, loc),
			},
		},
		{
			kind: "segment",
		},
		{
			kind: "point",
			pt: tk.Point{
				HasLoc:  true,
				Lat:     60.19325801,
				Lon:     24.93516301,
				HasElev: false,
				HasTime: true,
				Time: time.Date(2009, 2, 14, 14, 1, 7,
					0, loc),
			},
		},
	}

	se := sampleExpectation{
		t:       t,
		exp:     exp,
		llPrec:  llPrec,
		ePrec:   ePrec,
		hasName: hasName,
	}
	r := rc(readerCBs{
		Track:   se.track,
		Segment: se.segment,
		Point:   se.point,
	})

	f, err := os.Open(fn)
	if err != nil {
		t.Error(err)
		return
	}
	defer f.Close()
	err = r.Parse(f)
	if err != nil {
		t.Error(err)
		return
	}
	if se.i != len(exp) {
		t.Errorf("expected %d events, got %d", len(exp), se.i)
	}
}

func testSample(t *testing.T, fn string, llPrec, ePrec float64, hasName bool) {
	t.Helper()

	t.Run("sax", func(t *testing.T) {
		cr := func(cbs readerCBs) reader {
			r := GoSAXReader{}
			r.TrackCB = cbs.Track
			r.SegmentCB = cbs.Segment
			r.PointCB = cbs.Point
			return r
		}
		t.Run(fn, func(t *testing.T) {
			testSampleReally(t, cr, fn, llPrec, ePrec, hasName)
		})
	})

	t.Run("stdlib", func(t *testing.T) {
		cr := func(cbs readerCBs) reader {
			r := StdlibReader{}
			r.TrackCB = cbs.Track
			r.SegmentCB = cbs.Segment
			r.PointCB = cbs.Point
			return r
		}
		t.Run(fn, func(t *testing.T) {
			testSampleReally(t, cr, fn, llPrec, ePrec, hasName)
		})
	})
}

type sampleConfig struct {
	fn       string
	locPrec  float64
	elevPrec float64
	hasName  bool
}

var samples = []sampleConfig{
	{
		"samples/basic_air_data_gps_logger.gpx",
		0.0000001, 0.001, true,
	},
	{
		"samples/maemo-mapper.gpx",
		0.000001, 0.01, false,
	},
	{
		"samples/mtkbabel.gpx",
		0.0000001, 0.001, true,
	},
}

func TestGoSAXSamples(t *testing.T) {
	for _, s := range samples {
		t.Run(s.fn, func(t *testing.T) {
			cr := func(cbs readerCBs) reader {
				r := GoSAXReader{}
				r.TrackCB = cbs.Track
				r.SegmentCB = cbs.Segment
				r.PointCB = cbs.Point
				return r
			}
			testSampleReally(t, cr, s.fn, s.locPrec, s.elevPrec,
				s.hasName)
		})
	}
}

func TestStdlibSamples(t *testing.T) {
	for _, s := range samples {
		t.Run(s.fn, func(t *testing.T) {
			cr := func(cbs readerCBs) reader {
				r := StdlibReader{}
				r.TrackCB = cbs.Track
				r.SegmentCB = cbs.Segment
				r.PointCB = cbs.Point
				return r
			}
			testSampleReally(t, cr, s.fn, s.locPrec, s.elevPrec,
				s.hasName)
		})
	}
}

func TestSamples(t *testing.T) {
	testSample(t, "samples/basic_air_data_gps_logger.gpx",
		0.0000001, 0.001, true)
	testSample(t, "samples/maemo-mapper.gpx", 0.000001, 0.01, false)
	testSample(t, "samples/mtkbabel.gpx", 0.0000001, 0.001, true)
}

// generate gpx data at least n bytes in size
func genGPX(n int) string {
	out := `<?xml version="1.0" encoding="UTF-8"?>
<gpx version="1.1" creator="gpxtoolkit"
     xmlns="http://www.topografix.com/GPX/1/1"
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
<trk>
  <trkseg>`
	lat := 10.0
	lon := 10.0
	elev := 0.0
	t := time.Date(2022, 8, 1, 11, 00, 57, 0,
		time.FixedZone("Z", 0))
	for len(out) < n {
		out += fmt.Sprintf(`<trkpt lat="%.6f" lon="%.6f"><ele>%.3f</ele><time>%s</time><sat>7</sat></trkpt>`,
			lat, lon, elev, t.Format(time.RFC3339))
		lat += 0.000001
		lon += 0.000001
		elev += 0.01
		t = t.Add(1 * time.Second)
	}
	out += "</trkseg></trk>"
	return out
}

func benchmarkGoSAXReader(b *testing.B, size int) {
	b.Helper()

	tmp, err := ioutil.TempFile(os.TempDir(), "gpx")
	if err != nil {
		b.Error(err)
		return
	}
	tmp.Write([]byte(genGPX(size)))
	tmp.Sync()
	defer os.Remove(tmp.Name())
	b.ResetTimer()

	r := GoSAXReader{
		ReaderCallbacks: ReaderCallbacks{
			TrackCB:   func(n string) {},
			SegmentCB: func() {},
			PointCB:   func(pt *tk.Point) {},
		},
	}
	for i := 0; i < b.N; i++ {
		r.ParseFile(tmp.Name())
	}
}

func benchmarkStdlibReader(b *testing.B, size int) {
	b.Helper()

	io := strings.NewReader(genGPX(size))
	b.ResetTimer()

	r := StdlibReader{
		ReaderCallbacks: ReaderCallbacks{
			TrackCB:   func(n string) {},
			SegmentCB: func() {},
			PointCB:   func(pt *tk.Point) {},
		},
	}
	for i := 0; i < b.N; i++ {
		r.Parse(io)
		io.Seek(0, 0)
	}
}

func BenchmarkGoSAXReader10KiB(b *testing.B) {
	benchmarkGoSAXReader(b, 10_230)
}

func BenchmarkGoSAXReader100KiB(b *testing.B) {
	benchmarkGoSAXReader(b, 102_400)
}

func BenchmarkGoSAXReader1000KiB(b *testing.B) {
	benchmarkGoSAXReader(b, 1_024_000)
}

func BenchmarkStdlibReader10KiB(b *testing.B) {
	benchmarkStdlibReader(b, 10_230)
}

func BenchmarkStdlibReader100KiB(b *testing.B) {
	benchmarkStdlibReader(b, 102_400)
}

func BenchmarkStdlibReader1000KiB(b *testing.B) {
	benchmarkStdlibReader(b, 1_024_000)
}
