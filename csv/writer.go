package csv

import (
	"fmt"
	"io"

	tk "gitlab.com/tsaviran/gpxtoolkit"
)

// FprintPoint prints point to writer.
func FprintPoint(w io.Writer, pt tk.Point) error {
	if _, err := fmt.Fprintf(w, "%f,%f", pt.Lon, pt.Lat); err != nil {
		return err
	}
	if pt.HasTime {
		ts := float64(pt.Time.UnixNano()) / 1_000_000_000.0
		if _, err := fmt.Fprintf(w, ",T,%f", ts); err != nil {
			return err
		}
	}
	if pt.HasElev {
		if _, err := fmt.Fprintf(w, ",E,%f", pt.Elev); err != nil {
			return err
		}
	}
	return nil
}
