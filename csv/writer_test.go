package csv

import (
	"bytes"
	"encoding/csv"
	"strings"
	"testing"
	"time"

	tk "gitlab.com/tsaviran/gpxtoolkit"
)

func TestFprintPoint(t *testing.T) {
	loc := time.FixedZone("some", 5400)
	tests := []struct {
		name   string
		pt     tk.Point
		latlon string
		parts  []string
	}{
		{
			"latlon",
			tk.Point{Lon: 12.34, Lat: 23.45, HasLoc: true},
			"12.340000,23.450000",
			[]string{},
		},
		{
			"time",
			tk.Point{
				Lon: 12.34, Lat: 23.45, HasLoc: true,
				Time: time.Date(2022, 7, 29, 14, 36, 9,
					120_000_000, loc),
				HasTime: true,
			},
			"12.340000,23.450000",
			[]string{"T,1659099969.120000"},
		},
		{
			"elev",
			tk.Point{
				Lon: 12.34, Lat: 23.45, HasLoc: true,
				Elev: 34.56, HasElev: true,
			},
			"12.340000,23.450000",
			[]string{"E,34.560000"},
		},
		{
			"neg",
			tk.Point{Lon: -12.34, Lat: -23.45, HasLoc: true,
				Elev: -34.56, HasElev: true},
			"-12.340000,-23.450000",
			[]string{"E,-34.560000"},
		},
		{
			"time+elev",
			tk.Point{
				Lon: 12.34, Lat: 23.45, HasLoc: true,
				Elev: 34.56, HasElev: true,
				Time: time.Date(2022, 7, 29, 14, 36, 9,
					120_000_000, loc),
				HasTime: true,
			},
			"12.340000,23.450000",
			[]string{
				"T,1659099969.120000",
				"E,34.560000",
			},
		},
	}

	for _, tst := range tests {
		t.Run(tst.name, func(t *testing.T) {
			b := bytes.Buffer{}
			err := FprintPoint(&b, tst.pt)
			if err != nil {
				t.Error(err)
			}
			r := csv.NewReader(&b)
			fields, err := r.Read()
			if err != nil {
				t.Error(err)
				return
			}
			if len(fields) < 2 {
				t.Errorf("no latlon present in %q", b.String())
				return
			}

			latlon := strings.Join(fields[0:2], ",")
			if tst.latlon != latlon {
				t.Errorf("expected latlon %q, got %q", tst.latlon, latlon)
			}

			for _, eb := range tst.parts {
				match := false
				for i := 2; i < len(fields); i += 2 {
					bit := strings.Join(fields[i:i+2], ",")
					if bit == eb {
						match = true
						break
					}
				}
				if !match {
					t.Errorf("expected bit %q not in %q", eb, b.String())
				}
			}
		})
	}
}
