package csv

import (
	gocsv "encoding/csv"
	"fmt"
	"io"
	"math"
	"strconv"
	"time"

	tk "gitlab.com/tsaviran/gpxtoolkit"
)

// Parse returns a gpxtoolkit.Point and error, if any.
func Parse(rec []string) (tk.Point, error) {
	var err error
	pt := tk.Point{}

	if len(rec) < 2 {
		return pt, fmt.Errorf("too few columns (%d)", len(rec))
	}

	if pt.Lon, err = strconv.ParseFloat(rec[0], 64); err != nil {
		return pt, fmt.Errorf("bad lon: %q (%s)", rec[1], err.Error())
	}

	if pt.Lat, err = strconv.ParseFloat(rec[1], 64); err != nil {
		return pt, fmt.Errorf("bad lat: %q (%s)", rec[0], err.Error())
	}
	pt.HasLoc = true

	for i := 2; i+1 < len(rec); i += 2 {
		switch rec[i] {
		case "T":
			if v, err := strconv.ParseFloat(rec[i+1], 64); err == nil {
				sec, ns := math.Modf(v)
				t := time.Unix(int64(sec), int64(ns*1_000_000_000))
				pt.Time = t
				pt.HasTime = true
			} else {
				return pt, fmt.Errorf("bad ts: %q (%s)", rec[i+1], err.Error())
			}
		case "E":
			if v, err := strconv.ParseFloat(rec[i+1], 64); err == nil {
				pt.Elev = v
				pt.HasElev = true
			} else {
				return pt, fmt.Errorf("bad elev: %q (%s)", rec[i+1], err.Error())
			}
		default:
			return pt, fmt.Errorf("unexpected key: %q", rec[i])
		}
	}
	return pt, nil
}

// EachPoint calls a function for each read Point.
func EachPoint(r io.Reader, cb func(pt tk.Point)) error {
	csvr := gocsv.NewReader(r)
	csvr.FieldsPerRecord = -1
	for {
		rec, err := csvr.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		pt, err := Parse(rec)
		if err != nil {
			return err
		}
		cb(pt)

	}
	return nil
}
