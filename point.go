package gpxtoolkit

import "time"

// Point represents a geographic location, with timestamp.
type Point struct {
	Lat     float64
	Lon     float64
	Elev    float64
	Time    time.Time
	HasLoc  bool
	HasElev bool
	HasTime bool
}

// Invalidate clears point of its details.
func (pt *Point) Invalidate() {
	pt.HasLoc = false
	pt.HasElev = false
	pt.HasTime = false
}
